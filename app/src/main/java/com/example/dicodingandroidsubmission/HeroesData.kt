package com.example.dicodingandroidsubmission

object HeroesData {
    private val heroNames = arrayOf(
        "Labuan Bajo, NTT",
        "Raja Ampat, Papua Barat",
        "Kepulauan Derawan, Kalimantan Timur",
        "Candi Borobudur, Jawa Tengah",
        "Gunung Rinjani, NTB",
        "Gili Trawangan, NTB",
        "Tana Toraja, Sulawesi Selatan",
        "Pulau Sumba, NTT",
        "Wakatobi, Sulawesi Tenggara",
        "Taman Nasional Bali Barat"
        )

    private val heroDetails = arrayOf(
        "Labuan Bajo menjadi salah satu wisata di Indonesia yang menjadi impian sebagian besar orang. Destinasi ini menghadirkan berbagai paket wisata yang lengkap dan menawan, mulai dari destinasi bahari hingga darat.",
        "Sahabat pasti sudah tidak asing dengan wisata di Indonesia yang satu ini. Pasalnya, daya tarik Raja Ampat tak hanya diminati oleh wisatawan lokal saja, tetapi juga sudah menarik hati para wisatawan dari berbagai belahan dunia lainnya. ",
        "Jika Papua memiliki surga tersembunyi di Raja Ampat, Kalimantan Timur justru memiliki surga tersembunyi di Kepulauan Derawan. Keindahan Pulau Derawan tak sebatas pada pasir pantai yang putih dan air laut yang sangat jernih, tetapi juga destinasi bawah lautnya pun tak kalah menawan.",
        "Candi Borobudur menjadi salah satu warisan budaya yang menjadi kebanggaan Indonesia. Berbeda dari wisata di Indonesia yang sebelumnya disebutkan, Candi Borobudur termasuk destinasi yang lebih mudah dijangkau oleh banyak kalangan. ",
        "Gunung Rinjani termasuk salah satu wisata di Indonesia yang menjadi impian bagi sebagian besar pendaki. Nama gunung rinjani semakin dikenal setelah menjadi salah satu latar tempat pada film berjudul 5 cm.",
        "Di Nusa Tenggara Barat, Sahabat tak hanya bisa menikmati keindahan pegunungan saja, tetapi juga pantai yang membentang dengan indah dan eksotis. Gili Trawangan menjadi salah satu wisata di Indonesia yang favorit, letaknya di desa Gili Indah, kecamatan Pemenang, Lombok Utara, Nusa Tenggara Barat. ",
        "Tempat wisata yang satu ini sudah tidak perlu dipertanyakan lagi daya tariknya. Mulai dari wisata kebudayaan hingga wisata alam, semuanya hadir di Tana Toraja dengan keindahan dan keunikan sendiri yang tidak dimiliki oleh destinasi lain.",
        "Beberapa tahun terakhir, pulau Sumba menjadi salah satu tempat wisata di Indonesia yang semakin dilirik oleh wisatawan mancanegara, salah satunya adalah pantai Nihiwatu. Daya tarik pantai ini semakin dilirik oleh banyak orang berkat sebagian artis Indonesia yang memilih liburan di sana.",
        "Satu lagi destinasi wisata di Indonesia yang paling ikonik, yaitu Wakatobi yang terletak di Sulawesi Tenggara. Wakatobi terkenal akan keindahan bawah laut dan biota lautnya yang beragam. ",
        "Bali tak hanya terkenal dengan pantainya. Taman Nasional Bali Barat yang memiliki sabana, pantai, dan hutan, juga sangat direkomendasikan untuk dikunjungi."
        )

    private val heroesImages = intArrayOf(
        R.drawable.labuan_bajo,
        R.drawable.raja_ampat,
        R.drawable.kepulauan_derawan,
        R.drawable.candi_borobudur,
        R.drawable.gunung_rinjani,
        R.drawable.gili_trawangan,
        R.drawable.tana_toraja,
        R.drawable.pulau_sumba,
        R.drawable.wakatobi,
        R.drawable.taman_nasional_bali)

    val listData: ArrayList<Hero>
        get() {
            val list = arrayListOf<Hero>()
            for (position in heroNames.indices) {
                val hero = Hero()
                hero.name = heroNames[position]
                hero.detail = heroDetails[position]
                hero.photo = heroesImages[position]
                list.add(hero)
            }
            return list
        }
}