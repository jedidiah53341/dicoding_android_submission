package com.example.dicodingandroidsubmission

data class Hero(
    var name: String = "",
    var detail: String = "",
    var photo: Int = 0
)