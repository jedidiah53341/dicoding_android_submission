package com.example.dicodingandroidsubmission

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.dicodingandroidsubmission.databinding.ActivityMoveWithDataBinding

class MoveWithDataActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PHOTO = "extra_photo"
        const val EXTRA_DETAIL = "extra_age"
        const val EXTRA_NAME = "extra_name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_data)

        val name_data: TextView = findViewById(R.id.extra_name)
        val photo_data: ImageView = findViewById(R.id.extra_photo)
        val detail_data: TextView = findViewById(R.id.extra_detail)


        val name = intent.getStringExtra(EXTRA_NAME)
        val detail = intent.getStringExtra(EXTRA_DETAIL)
        val photo = intent.getStringExtra(EXTRA_PHOTO)

        name_data.text = name
        detail_data.text = detail

        Glide.with(this)
            .load(photo)
            .into(photo_data)

    }
}